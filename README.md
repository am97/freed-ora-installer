# freed-ora-installer

A script to simplify the installation of [freed-ora](http://www.fsfla.org/ikiwiki/selibre/linux-libre/freed-ora.en.html)

Freed-ora helps you remove any proprietary bits in a [Fedora](https://getfedora.org/) installation, making it 100% Free. 
This script has been tested on a fresh Fedora 29 installation, but should work on any Fedora version supported by Freed-ora.

## Disclaimer
This script comes with no warranty. Use it at your own risk

## Usage
```
git clone https://framagit.org/am97/freed-ora-installer.git
cd freed-ora-installer/
sudo ./install_freed-ora.sh 
```

## Tests
| Fedora version | Success? | Notes         |
|----------------|----------|---------------|
| 30             |          |               |
| 29             | Yes      | Fresh install |
| 28             | Yes      | Fresh install |
| 27             |          |               |

## Contributing
This script still needs testing! Do not hesitate to send pull requests :wink:
