# Check if script is running as root
#https://stackoverflow.com/questions/18215973/how-to-check-if-running-as-root-in-a-bash-script
if [ "$EUID" -ne 0 ]
  then echo -e "\e[31mError: This script needs root permissions.\e[0m"
  exit
fi

# Check if current kernel is kernel-libre
current_kernel=`uname -r | grep libre`
if [ -z "$current_kernel" ]; then #if current kernel is not kernel-libre

	# Import freed-ora repo
	echo -e "\e[32mImporting freed-ora repo...\e[0m"
	rpm --import http://linux-libre.fsfla.org/pub/linux-libre/SIGNING-KEY.linux-libre
	rpm -i http://linux-libre.fsfla.org/pub/linux-libre/freed-ora/freed-ora-release.noarch.rpm

	# Install kernel-libre
	# Note: kernel-headers is installed by default on Fedora 29, but a replacement package doesn't seems to exist
	echo -e "\e[32mInstalling kernel-libre...\e[0m"
	dnf install -y kernel-libre kernel-libre-modules kernel-libre-modules-extra kernel-libre-core || exit

	# Make kernel-libre the default kernel
	echo -e "\e[32mSetting kernel-libre as the default kernel...\e[0m"
	grubby --set-default=`ls -t /boot/vmlinuz-*libre* | sed 1q` || exit

	# Reboot before removing current kernel (otherwise, current kernel cannot be removed)
	echo -e "\e[32mYou must reboot before removing your current kernel.
	After rebooting, you should run this script again to complete installation.
	Do you want to reboot now? [y/N]:\e[0m"
	read answer
	# https://stackoverflow.com/questions/226703/how-do-i-prompt-for-yes-no-cancel-input-in-a-linux-shell-script/27875395#27875395
	# ${answer#[Yy]} uses parameter expansion to remove Y or y from the beginning of $answer, causing an inequality when either is present. This works in any POSIX shell (dash, ksh, bash, zsh, busybox, etc)
	if [ "$answer" != "${answer#[Yy]}" ] ;then
		echo -e "\e[32mRebooting...\e[0m"
		reboot
	else
		echo -e "\e[32mAborting...\e[0m"
		exit
	fi

else

	echo -e "\e[32mkernel-libre is the current kernel. We will now remove old kernel and other non-free software.\e[0m"

	# Remove non-kernel packages
	echo -e "\e[32mRemoving *-firmware and microcode_ctl*\e[0m"
	dnf remove --noautoremove -y *-firmware microcode_ctl*
	# Use rpm instead of dnf to remove kernel packages, to prevent other packages like gcc from being uninstalled
	echo -e "\e[32mRemoving old kernel...\e[0m"
	rpm -e --nodeps kernel kernel-core kernel-headers kernel-modules kernel-modules-extra

	# Install Freed-ora
	echo -e "\e[32mInstalling Freed-ora\e[0m"
	yum -y install freed-ora-freedom

	echo -e "\e[32mCongratulations, freed-ora is now installed !\e[0m"

fi